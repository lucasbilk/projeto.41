package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lucas
 */
public class Elipse {

    private double eixo1;
    private double eixo2;

    public Elipse() {
    }

    public Elipse(double eixo1, double eixo2) {
        this.eixo1 = eixo1;
        this.eixo2 = eixo2;
    }

    public double getArea() {
        return Math.PI * eixo1 * eixo2;
    }

    public double getPerimetro() {
        return Math.PI * (3 * (eixo1 + eixo2) - Math.sqrt((3 * eixo1 + eixo2) * (eixo1 + 3 * eixo2)));
    }
}
